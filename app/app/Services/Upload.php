<?php

namespace App\Services;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Storage;

class Upload
{
    private $file;
    private $path;

    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }
    public function upload()
    {
        $this->path = Storage::putFile('model', $this->file);
        return true;
    }

    public function path()
    {
        return $this->path;
    }

    public function name()
    {
        return $this->file->getClientOriginalName();
    }

    public function pathThumbnail()
    {
        return "images/thumbnail.svg";
    }

}
