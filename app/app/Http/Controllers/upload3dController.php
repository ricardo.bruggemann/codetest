<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FileUpload;
use App\Services\Upload;

class upload3dController extends Controller
{
    /**
     * Index
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(FileUpload::get());
    }

    /**
     * Update
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FileUpload $fileupload)
    {
        $validatedData = $request->validate($this->attributes());

        $file = new Upload($request->file('file'));
        if($file->upload())
        {
            $fileupload->name = $file->name();
            $fileupload->path = $file->path();
            $fileupload->path_thumbnail = $file->pathThumbnail();
            $fileupload->save();

            return response()->json(["msg" => "File updated"
                                    , "type" => "success"]);
        }
        else
        {
            return response()->json(["msg" => "File imported failed!"
                                    , "type" => "danger"]);
        }
    }

    /**
     * Import the file
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $validatedData = $request->validate($this->attributes());

        $file = new Upload($request->file('file'));
        if($file->upload())
        {
            $fileupload = new FileUpload();
            $fileupload->name = $file->name();
            $fileupload->path = $file->path();
            $fileupload->path_thumbnail = $file->pathThumbnail();
            $fileupload->save();

            return response()->json(["msg" => "File imported successfuly!"
                                    , "type" => "success"]);
        }
        else
        {
            return response()->json(["msg" => "File imported failed!"
                                    , "type" => "danger"]);
        }
    }

    /**
     * Destroy
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(FileUpload $fileupload)
    {
        $fileupload->delete();
        return response()->json(["msg" => "File deleted"
                                , "type" => "danger"]);
    }

    /**
     * Attributes to be validated
     *
     * @return \Illuminate\Http\Response
     */
    public function attributes() {
        return ['file' => 'required'];
    }

}
