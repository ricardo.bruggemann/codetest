<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InsurranceUnitTest extends TestCase
{
    /**
     * Expect return 522 because age is null
     * "age": null,
     * @return void
     */
    public function test_age_null()
    {
        // Expect return 522 because age is null
        $this->post(route('insurrance.calculate'), [
            'dependents'=> 2,
            'house'=> ['ownership_status' => 'owned'],
            'income'=> '-10',
            'marital_status'=>'married',
            'risk_questions'=> [ 0, 1, 0],
            'vehicle' => ["year" => 2018]
        ])->assertStatus(422);
    }
}
