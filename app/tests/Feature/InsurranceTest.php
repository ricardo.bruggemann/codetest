<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InsurranceTest extends TestCase
{
    /**
     * Expect return correct values at json
     * "age": null,
     * @return void
     */
    public function test_insurrance()
    {
        $this->post(route('insurrance.calculate'), [
            'age' => 35,
            'dependents'=> 2,
            'house'=> ['ownership_status' => 'owned'],
            'income'=> 0,
            'marital_status'=>'married',
            'risk_questions'=> [ 0, 1, 0],
            'vehicle' => ["year" => 2018]
        ])->assertStatus(200)
            ->assertExactJson([
                'auto' => "regular",
                'disability' => "ineligible",
                'home' => "economic",
                'life' => "regular",
            ]);
    }
}
