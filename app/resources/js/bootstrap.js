window._ = require('lodash');


import Vue from 'vue';
import axios from 'axios';
import Upload3d from './views/Upload3d';

Vue.use(Upload3d);
Vue.component('Upload3d', Upload3d);

if (!window.Promise) {
 	window.Promise = Promise
}

window.Vue = Vue;
window.axios = axios;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
