<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/file', 'upload3dController@index')->name('upload3d.grid');
Route::post('/file/import', 'upload3dController@import')->name('upload3d.import');
Route::delete('/file/destroy/{fileupload}', 'upload3dController@destroy')->name('upload3d.destroy');
Route::patch('/file/update/{fileupload}', 'upload3dController@update')->name('upload3d.update');
