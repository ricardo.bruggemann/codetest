# Requisites
Docker

# Installation
1) Clone repository prepared with Docker and Application
```
git clone https://gitlab.com/ricardo.bruggemann/codetest.git
```
2) Inside folder up Docker
```
docker-compose up
```

3) Connect workspace
```
docker-compose exec workspace bash
```

4) Download Laravel libraries
```
composer install
```

5) Config .env
```
APP_NAME=CodeTest
APP_ENV=local
APP_KEY=base64:HL/d4l+ntQIp8k2vlZoDqETO0ManiyVWc35wNKpkCIg=
APP_DEBUG=true
APP_URL=http://codetest.test

LOG_CHANNEL=stack
DB_CONNECTION=sqlite
DB_DATABASE=/var/www/app/database/database.sqlite
DB_FOREIGN_KEYS=false

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120
```

6) Add this line to hosts file
```
127.0.0.1 codetest.test
```

7) Access http://codetest.test


8) Picture
![alt text](https://gitlab.com/ricardo.bruggemann/codetest/uploads/50dcd0c2dfcd4a0a7cac297188357ac6/screen.PNG)
